python-djangosaml2 (1.9.3-1) unstable; urgency=low

  * New upstream version 1.9.3

 -- Michael Fladischer <fladi@debian.org>  Sun, 26 May 2024 20:37:10 +0000

python-djangosaml2 (1.9.2-1) unstable; urgency=low

  * New upstream version 1.9.2
  * Refresh patches.

 -- Michael Fladischer <fladi@debian.org>  Wed, 20 Mar 2024 08:57:10 +0000

python-djangosaml2 (1.9.0-1) unstable; urgency=low

  * New upstream version 1.9.0
  * Refresh patches.
  * Update year in d/copyright.

 -- Michael Fladischer <fladi@debian.org>  Wed, 10 Jan 2024 19:30:01 +0000

python-djangosaml2 (1.8.0-1) unstable; urgency=medium

  * New upstream version 1.8.0

 -- Michael Fladischer <fladi@debian.org>  Sat, 28 Oct 2023 08:40:54 +0000

python-djangosaml2 (1.7.0-1) unstable; urgency=medium

  * New upstream version 1.7.0

 -- Michael Fladischer <fladi@debian.org>  Fri, 07 Jul 2023 07:01:20 +0000

python-djangosaml2 (1.6.0-1) unstable; urgency=medium

  * New upstream version 1.6.0

 -- Michael Fladischer <fladi@debian.org>  Fri, 23 Jun 2023 06:28:10 +0000

python-djangosaml2 (1.5.8-1) unstable; urgency=medium

  * New upstream version 1.5.8
  * Bump Standards-Version to 4.6.2.
  * Build using pybuild-plugin-pyproject.
  * Refresh patches.

 -- Michael Fladischer <fladi@debian.org>  Mon, 19 Jun 2023 16:37:51 +0000

python-djangosaml2 (1.5.5-1) unstable; urgency=low

  * New upstream release.
  * Update d/copyright with new years.

 -- Michael Fladischer <fladi@debian.org>  Fri, 06 Jan 2023 20:14:47 +0000

python-djangosaml2 (1.5.3-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 23:28:53 +0100

python-djangosaml2 (1.5.3-1) unstable; urgency=low

  * New upstream release.
  * Use github tags instead of releases for d/watch.
  * Bump Standards-Version to 4.6.1.0.

 -- Michael Fladischer <fladi@debian.org>  Tue, 27 Sep 2022 12:35:03 +0000

python-djangosaml2 (1.5.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Sun, 03 Apr 2022 14:28:22 +0000

python-djangosaml2 (1.3.6-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.

 -- Michael Fladischer <fladi@debian.org>  Tue, 08 Feb 2022 09:03:30 +0000

python-djangosaml2 (1.3.5-1) unstable; urgency=low

  * New upstream release.
  * Depend on python3-all for autopkgtests.
  * Use django package directly to run tests rather than
    tests/run_tests.py.
  * Update year in d/copyright.
  * Add patch to skip test_login_authn_context if pysaml2 is < 7.1.

 -- Michael Fladischer <fladi@debian.org>  Fri, 28 Jan 2022 21:45:11 +0000

python-djangosaml2 (1.3.3-1) unstable; urgency=low

  * New upstream release.
  * Update project URL in d/watch.
  * Remove patches, fixed upstream.
  * Change README.rst to README.md.
  * Bump Standards-Version to 4.6.0.
  * Use uscan version 4.
  * Enable upstream testsuite for autopkgtests.
  * Remove unnecessary autopkgtest-pkg-python testsuite.

 -- Michael Fladischer <fladi@debian.org>  Sun, 29 Aug 2021 14:48:53 +0000

python-djangosaml2 (0.50.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Fri, 16 Oct 2020 18:30:45 +0200

python-djangosaml2 (0.40.1-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Michael Fladischer ]
  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Thu, 15 Oct 2020 11:26:13 +0200

python-djangosaml2 (0.40.0-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.

 -- Michael Fladischer <fladi@debian.org>  Thu, 06 Aug 2020 20:39:45 +0200

python-djangosaml2 (0.20.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Sun, 05 Jul 2020 21:05:26 +0200

python-djangosaml2 (0.19.1-1) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Michael Fladischer ]
  * New upstream release.
  * Refresh patches.
  * Bump debhelper version to 13.

 -- Michael Fladischer <fladi@debian.org>  Mon, 15 Jun 2020 21:34:24 +0200

python-djangosaml2 (0.18.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Michael Fladischer ]
  * New upstream release.
  * Add debian/gbp.conf.
  * Add patch to canonicalize (C14N) XML structures before comparing
    them for Python >= 3.8 (Closes: #950049).
  * Bump Standards-Version to 4.5.0.
  * Bump debhelper version to 12.
  * Set Rules-Requires-Root: no.

 -- Michael Fladischer <fladi@debian.org>  Thu, 20 Feb 2020 09:29:05 +0100

python-djangosaml2 (0.17.2-1) unstable; urgency=low

  * Initial release (Closes: #909432).

 -- Michael Fladischer <fladi@debian.org>  Sun, 23 Sep 2018 19:43:23 +0200
